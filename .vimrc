"Install Plug plugin manager by following instructions on
"https://github.com/junegunn/vim-plug or by using a package manager, for
"example on Arch Linux, you can install the vim-plug AUR package, by running paru -S vim-plug

"For rust support install rustup from https://rustup.rs
"Or for example on arch linux, by running pacman -S rustup
"Then install the latest stable release of rust by running
"rustup install stable
"Next install required rust components for the Coc extensions to work
"properly by running rustup component add rust-analyzer rust-analysis rust-src

"For Coc to be able to install all extensions, install nodejs, npm and yarn.
"https://nodejs.org/en for nodejs and npm. Choose the LTS version.
"https://yarnpkg.com/getting-started/install for yarn.
"On Arch Linux, you don't need to install anything manually, just run
"pacman -S nodejs-lts-gallium npm yarn
"and pacman installs everything for you.


"For coc-java and coc-kotlin to work, you need to install
"the java development kit.
"On windows you can get it here: https://adoptium.net/releases.html
"On Arch Linux you can just run pacman -S jdk-openjdk
"and again, pacman does everything for you.
"For kotlin, also install kotlin-language-server.
"On windows you can get it here: https://github.com/fwcd/kotlin-language-server
"On Arch Linux, there is a ready package in the AUR, so you can install it by
"running this command: paru -S kotlin-language-server
"Then in coc-settings.json (you can open the file with :CocConfig), add this
"to the languageserver section or create one if it doesn't exist:
"{
"    "languageserver": {
"        "kotlin": {
"            "command":
"                "/usr/bin/kotlin-language-server",
"            "filetypes":
"                ["kotlin"]
"        }
"    }
"}


"For coc-omnisharp to work, you need to install the dotnet SDK.
"For instructions, see here: https://docs.microsoft.com/en-us/dotnet/core/install
"On Arch Linux you can just run pacman -S dotnet-sdk
"After it is installed, run this command to install csharp language server:
"dotnet tool install --global csharp-ls 
"Then follow the instructions on screen, specifically adding
"$HOME/.dotnet/tools to your PATH variable
"Next, inside vim, run :CocConfig and add add these lines:
"{
"    "languageserver": {
"        "csharp-ls": {
"            "command": "csharp-ls",
"            "filetypes": ["cs"],
"            "rootPatterns": ["*.csproj", ".vim/", ".git/", ".hg/"]
"        }
"   }
"}


"So for example if you have both csharp-ls and kotlin configured in
"coc-settings.json, it should look something like this:
"{
"    "languageserver": {
"        "csharp-ls": {
"            "command": "csharp-ls",
"            "filetypes": ["cs"],
"            "rootPatterns": ["*.csproj",
"                ".vim/", ".git/", ".hg/"]
"        },
"        "kotlin": {
"            "command":
"                "/usr/bin/kotlin-language-server",
"            "filetypes":
"                ["kotlin"]
"        }
"    }
"}


"For vim-godot to work with coc.nvim, you should add a godot section in to the
"lanugageserver section in coc-settings.json:
"{
"    "languageserver": {
"        "godot": {
"            "host": "127.0.0.1",
"            "filetypes": ["gdscript"],
"            "port": 6005
"        }
"   }
"}

"Now inside Godot Engine, go to Editor/Editor Settings/Text Editor/External
"Then turn on "Use External Editor", set "Exec Path" to vim/nvim executable,
"For example /usr/bin/nvim
"Change "Exec Flags" to look like this if using vim:
" --servername godot --remote-send "<C-\><C-N>:n {file}<CR>{line}G{col}|"
"And if using nvim, change it to this:
" --server ./godothost --remote-send "<C-\><C-N>:n {file}<CR>{line}G{col}|"

"Inside your Godot project's directory (where you have project.godot), open
"nvim like this: nvim --listen ./godothost
"Then when you open some script inside Godot Engine, it will open in vim
"instead of the built-in editor.


"After installing all required dependencies, inside vim
"run :PlugInstall to install all the plugins below.
"Occasionally run :PlugUpdate to update all your plugins, and :PlugUpgrade to
"update Plug plugin manager itself. If you installed Plug on Linux via your
"distro's package manager, you shouldn't need to run :PlugUpgrade because your
"package manager handles updating Plug for you.

call plug#begin('~/.vim/plugged')
" Plugins go here:

"Coc extensions
"This installs coc.nvim extensions automatically
let g:coc_global_extensions=['coc-git', 'coc-java', 'coc-kotlin', 'coc-dash-complete', 'coc-dot-complete', 'coc-just-complete', 'coc-json', 'coc-jedi', 'coc-lightbulb', 'coc-rust-analyzer', 'coc-sh', 'coc-vimlsp', 'coc-yaml', 'coc-toml', 'coc-tsserver', 'coc-omnisharp', 'coc-clangd']
"To manually install coc.nvim extensions, run :CocInstall coc-git coc-java coc-dash-complete coc-dot-complete coc-just-complete coc-json coc-jedi coc-lightbulb coc-rust-analyzer coc-sh coc-vimlsp coc-yaml coc-kotlin coc-toml coc-tsserver coc-omnisharp coc-clangd

" Occasionally run :CocUpdate to update all the Coc extensions.
Plug 'neoclide/coc.nvim', { 'branch': 'release' }

" vim-rooter changes your working dir to the one the current file is in.
Plug 'airblade/vim-rooter'

" Support for different programming languages
Plug 'sheerun/vim-polyglot'

"better surroundings for quotes, brackets etc.
Plug 'tpope/vim-surround'

"Better . (repeat) functionality with plugins
Plug 'tpope/vim-repeat'

" better statusbar
Plug 'itchyny/lightline.vim'

" Highlight yanked area for a brief time
Plug 'machakann/vim-highlightedyank'

" Fugitive Git tool
Plug 'tpope/vim-fugitive'

" NerdTree
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

" Afterglow colorscheme. Looks nice in a terminal emulator but looks terrible
" in a TTY, so keep that in mind
Plug 'danilo-augusto/vim-afterglow'

" Show lines at indentation points for easier reading
Plug 'Yggdroot/indentLine'

" Comment/uncomment with gcc and gc
Plug 'tpope/vim-commentary'

" A tagbar which shows variables, imports etc.
Plug 'preservim/tagbar'

" Godot support
Plug 'habamax/vim-godot'

" Run commands asynchronously with :AsyncRun
Plug 'skywind3000/asyncrun.vim'

" Gradle integration
Plug 'hdiniz/vim-gradle'

" All of your Plugins must be added before the following line
call plug#end()

" Put your non-Plugin stuff after this line

" Automatically format .kt and .kts files on save.
" In order for this to work, add the following line to the plugins section in your project's build.gradle.kts:
" id("org.jmailen.kotlinter") version "4.3.0"

" You may also want to add these lines to your project's .editorconfig to enforce a specific
" code style (refer to https://github.com/jeremymailen/kotlinter-gradle?#editorconfig for more information):
" [*.{kt,kts}]
" ktlint_code_style = ktlint_official

autocmd BufWritePost *.kt,*.kts Gradle formatKotlin

" check for external changes to the current file while cursor is stationary
" for at least 2 seconds
au CursorHold * :call timer_start(2000, { -> execute('checktime')}, { 'repeat': -1 })

" Godot executable path
" Linux
let g:godot_executable = '/usr/bin/godot'
" macOS"
"let g:godot_executable = '/Applications/Godot.app'
" Windows
"let g:godot_executable = 'C:\Path\To\godot.exe'

" Map leader keys
let mapleader=' '
let maplocalleader=' '

" toggle nerdtree
function ToggleNerdTree()
    if exists("g:NERDTree") && g:NERDTree.IsOpen()
        NERDTreeClose
    else
        NERDTreeRefreshRoot
        NERDTreeFocus
    endif
endfunction

nnoremap <Leader>n :call ToggleNerdTree()<cr>

" Toggle visibility of tag bar
nnoremap <Leader>t :TagbarToggle<cr>

" Add vim's own termdebug
packadd! termdebug

" Set colorscheme
"colorscheme afterglow
colorscheme candy

" Code folding/unfolding with za, zR, zo and zc
"set foldmethod=syntax
set foldmethod=indent   
set foldnestmax=20
set nofoldenable
set foldlevel=6

" enable syntax highlighting
syntax on
"enable line numbers
set number

" indenting settings
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use 4 spaces width
set shiftwidth=4
" On pressing tab, insert 4 spaces
set expandtab

" virtual tabstops using spaces
set shiftwidth=4
set softtabstop=4
set expandtab

" allow toggling between local and default mode
function TabToggle()
  if &expandtab
    set shiftwidth=4
    set softtabstop=0
    set noexpandtab
  else
    set shiftwidth=4
    set softtabstop=4
    set expandtab
  endif
endfunction
nmap <F9> mz:execute TabToggle()<CR>'z


" Use when file type is Godot's gdscript
func! GodotSettings() abort
    setlocal foldmethod=expr
    setlocal tabstop=4
    nnoremap <buffer> <F4> :GodotRunLast<CR>
    nnoremap <buffer> <F5> :GodotRun<CR>
    nnoremap <buffer> <F6> :GodotRunCurrent<CR>
    nnoremap <buffer> <F7> :GodotRunFZF<CR>
endfunction
augroup godot | au!
    au FileType gdscript call GodotSettings()
augroup end


"---------------------------------------------------------------------------------------------------------
"Coc.nvim configurations. Copied from https://github.com/neoclide/coc.nvim#example-vim-configuration
" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4 s) leads to noticeable
" delays and poor user experience.
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
      \ coc#pum#visible() ? coc#pum#next(1) :
      \ CheckBackspace() ? "\<Tab>" :
      \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! CheckBackspace() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call ShowDocumentation()<CR>

function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying code actions to the selected code block.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for apply code actions at the cursor position.
nmap <leader>ac  <Plug>(coc-codeaction-cursor)
" Remap keys for apply code actions affect whole buffer.
nmap <leader>as  <Plug>(coc-codeaction-source)
" Apply the most preferred quickfix action to fix diagnostic on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Remap keys for apply refactor code actions.
nmap <silent> <leader>re <Plug>(coc-codeaction-refactor)
xmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)
nmap <silent> <leader>r  <Plug>(coc-codeaction-refactor-selected)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
