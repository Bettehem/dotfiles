zshcache_time="$(date +%s%N)"
autoload -Uz add-zsh-hook
rehash_precmd() {
    if [[ -a /var/cache/zsh/pacman ]]; then
        local paccache_time="$(date -r /var/cache/zsh/pacman +%s%N)"
        if (( zshcache_time < paccache_time )); then
            rehash
            zshcache_time="$paccache_time"
        fi
    fi
}
add-zsh-hook -Uz precmd rehash_precmd

autoload -Uz compinit
zstyle ':completion:*' menu select
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'
#zstyle ':completion:*' rehash true
zmodload zsh/complist
compinit

source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

autoload bashcompinit
bashcompinit

bindkey "${terminfo[khome]}" beginning-of-line
bindkey "${terminfo[kend]}" end-of-line
bindkey -v

eval "$(register-python-argcomplete pmbootstrap)"

# Add local bin to path
export PATH="$HOME/.local/bin:$PATH"

#add dotnet tools to path
export PATH="$PATH:$HOME/.dotnet/tools"
# Opt out of dotnet telemetry
export DOTNET_CLI_TELEMETRY_OPTOUT=1
# zsh tab completion for the dotnet CLI
_dotnet_zsh_complete() {
    local completions=("$(dotnet complete "$words")")
    reply=( "${(ps:\n:)completions}" )
}
compctl -K _dotnet_zsh_complete dotnet


# docker export for Docker rootless mode
export DOCKER_HOST=unix://$XDG_RUNTIME_DIR/docker.sock

#Add gamemode to LD_PRELOAD
#export LD_PRELOAD="$LD_PRELOAD:/usr/lib/libgamemodeauto.so.0"

# Show splash with system info
#neofetch | lolcat
#archey3 | lolcat
#ufetch | lolcat


# Set ZSH prompt style
#prompt grml-large
#prompt adam2
prompt grml

# Set urxvt colors
# Set background and foreground color
#export BG_COLOR=Black
export BG_COLOR=rgba:0000/0000/0200/c800
#export BG_COLOR=rgba:0000/0000/0000/4444
export FG_COLOR=White
# Set scroll bar position
#-sr will set it to the right side and +sr will set it to the left side
#export SCROLL_BAR_POS='-sr'
export SCROLL_BAR_POS='+sb'

#export TERMINAL="urxvt -depth 32 -bg $BG_COLOR -fg $FG_COLOR $SCROLL_BAR_POS"
export TERMINAL=alacritty

# Use ccache when building android environment
export USE_CCACHE=1
export CCACHE_EXEC=/usr/bin/ccache
export CCACHE_COMPRESS=1
export ANDROID_JACK_VM_ARGS="-Dfile.encoding=UTF-8 -XX:+TieredCompilation -Xmx8G"

# Disable autostarting jackd
export JACK_NO_START_SERVER=1

# bash-insulter
if [ -f /etc/bash.command-not-found ]; then
	. /etc/bash.command-not-found
fi

# Wayland stuff
# Enable environment variables for wayland
enable_wayland(){
    export MOZ_ENABLE_WAYLAND=1
    export SDL_VIDEODRIVER="wayland"
    export CLUTTER_BACKEND="wayland"
    export QT_QPA_PLATFORM="wayland;xcb"
    export QT_QPA_PLATFORMTHEME="qt5ct"
    export QT_WAYLAND_DISABLE_WINDOWDECORATION=1
    export ECORE_EVAS_ENGINE="wayland"
    export ELM_ENGINE="wayland"
    export _JAVA_AWT_WM_NONREPARENTING=1
    export GDK_BACKEND="wayland"
}

steam-native() {
    #GDK_BACKEND=x11 command steam-native \$@
    GDK_BACKEND=wayland command steam-native $@
}

# Aliases

alias urxvt="urxvt -depth 32 -bg $BG_COLOR -fg $FG_COLOR $SCROLL_BAR_POS"

alias vim=nvim

alias cat=bat

alias tetris=drop4

alias sway="enable_wayland && sway \$@"

alias kernelcheck='echo -e "Installed version:\t\t$(pacman -Q linux)\nCurrently running version:\tl$(uname -rs | cut -dL -f2)"'

alias dots="git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"

alias ls=exa

alias cd=z

alias cherry="git cherry-pick -x"
alias ch=cherry

alias contains="git branch --contains"
alias co=contains

log-grep() {
    git log --grep="$@"
}

alias lo=log-grep

# Initialize zoxide
eval "$(zoxide init zsh)"

